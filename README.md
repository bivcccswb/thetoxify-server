**Setup Guide:**

You need to setup Deno and MongoDB to run this application server.

Install [deno](https://deno.land/manual/getting_started/installation).

Install [mongodb](https://docs.mongodb.com/manual/administration/install-community/).

Steps to run the server:

1. Run mongodb. Follow steps after installation in the above guide.
2. Run this command: `deno run --allow-all server.ts`

The server will start running on port 4000.

To test, goto [localhost:4000/api/blog](http://localhost:4000/api/blog)

You will see the following output:

**\{**`"success": true,"data": [<Your data here. Nothing if empty>]`**\}**