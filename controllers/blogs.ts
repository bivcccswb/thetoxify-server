import { MongoClient } from "https://deno.land/x/mongo@v0.22.0/mod.ts";
import { Blog } from "../types.ts";

const URI = "mongodb://127.0.0.1:27017";

// Mongo Connection Init
const client = new MongoClient();
try {
  await client.connect(URI);
  console.log("Database successfully connected");
} catch (err) {
  console.log(err);
}

const db = client.database("blogsApp"); 
const blogs = db.collection<Blog>("blogs");

// DESC: Add single blog
// METHOD: POST /api/blog

const addBlog = async ({
    request,
    response,
}: {
    request: any,
    response: any,
}) => {
    try {
        // If the request has no body, it will return 400
        if (!request.hasBody) {
            response.status = 400;
            response.body = {
                success: false,
                msg: "No Data",
            };
        } else {
            // Otherwise, it will try to insert 
            // a blog in the DB and respond with 201
            const body = await request.body();
            const blog = await body.value;
            await blogs.insertOne(blog);
            response.status = 201;
            response.body = {
                success: true,
                data: blog,
            };
        }
    } catch(err) {
        response.body = {
            success: false,
            msg: err.toString(),
        };
    }
}

// DESC: GET single blog
// METHOD: GET /api/blog/:id
const getBlog = async ({
    params,
    response,
}: {
    params: { id: string };
    response: any;
}) => {
    // Searches for a particular quote in the DB
    const blog = await blogs.findOne({ blogID: params.id });
    // If found, respond with the quote. If not, respond with a 404
    if (blog) {
        response.status = 200;
        response.body = {
            success: true,
            data: blog,
        };
    } else {
        response.status = 404;
        response.body = {
            success: false,
            msg: "No blog found",
        };
    }
};

// DESC: GET all Blogs
// METHOD GET /api/bloge
const getBlogs = async ({
    response
}: {
    response: any
}) => {
    try {
        // Find all blogs and convert them into an Array
        const allBlogs = await blogs.find({}).toArray();
        console.log(allBlogs);
        if (allBlogs) {
            response.status = 200;
            response.body = {
                success: true,
                data: allBlogs,
            };
        } else {
            response.status = 500;
            response.body = {
                success: false,
                msg: "Internal Server Error",
            };
        }
    } catch (err) {
        response.body = {
            success: false,
            msg: err.toString(),
        };
    }
};

// DESC: UPDATE single blog
// METHOD: PUT /api/blog/:id
const updateBlog = async ({
    params,
    request,
    response,
}: {
    params: { id: string };
    request: any;
    response: any;
}) => {
    try {
        // Search a blog in the DB and update with given values if found
        const body = await request.body();
        const inputBlog = await body.value;
        await blogs.updateOne(
            { blogID: params.id },
            { $set: { blog: inputBlog.blog, author: inputBlog.author } }
        );
        // Respond with the Updated Quote
        const updatedBlog = await blogs.findOne({ blogID: params.id });
        response.status = 200;
        response.body = {
            success: true,
            data: updatedBlog,
        };
    } catch (err) {
        response.body = {
            success: false,
            msg: err.toString(),
        };
    }
};

// DESC: DELETE single blog
// METHOD: DELETE /api/blog/:id
const deleteBlog = async ({
    params,
    response,
}: {
    params: { id: string };
    request: any;
    response: any;
}) => {
    try {
        // Search for the given blog and drop it from the DB
        await blogs.deleteOne({ blogID: params.id });
        response.status = 201;
        response.body = {
            success: true,
            msg: "Product deleted",
        };
    } catch (err) {
        response.body = {
            success: false,
            msg: err.toString(),
        };
    }
};

export { getBlogs, getBlog, addBlog, updateBlog, deleteBlog };