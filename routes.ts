import { Router } from "https://deno.land/x/oak/mod.ts";
import { getBlogs, getBlog, addBlog, updateBlog, deleteBlog } from "./controllers/blogs.ts";

const router = new Router(); // Create Router

router
  .get("/api/blog", getBlogs) // Get all blogs
  .get("/api/blog/:id", getBlog) // Get one blog of quoteID: id
  .post("/api/blog", addBlog) // Add a blog
  .put("/api/blog/:id", updateBlog) // Update a blog
  .delete("/api/blog/:id", deleteBlog); // Delete a blog

export default router;