export interface Blog {
    _id: { $oid: string },
    blog: string,
    blogID: string,
    author: string,
}